class Code
  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }
  attr_reader :pegs

  def self.parse(code_string)
    chars = code_string.chars.map(&:upcase)

    raise ArgumentError unless chars - PEGS.keys == []
    pegs = chars.map {|char| PEGS[char]}
    self.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample}
    self.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other)
    matches = 0
    4.times { |idx| matches += 1 if @pegs[idx] == other.pegs[idx]}
    matches
  end

  def near_matches(other)
    matches = 0
    PEGS.values.each do |color|
      matches += [@pegs.count(color), other.pegs.count(color)].min
    end
    matches - exact_matches(other)
  end

  def ==(other)
    return false unless other.class == Code
    @pegs == other.pegs
  end

  def to_s
    "[#{@pegs.join(", ")}]"
  end

end

class Game
  attr_reader :secret_code

  def initialize(code = nil)
    @secret_code = code || Code.random
  end

  def play
    10.times do |turn|
      get_guess
      break if won?(code)
    end
    puts "Sorry you lost..."
    puts "The code was #{@secret_code}"
  end

  def won?
    @secret_code == @current_code
  end

  def get_guess
    print 'Enter a guess (ex. "BGPR"): '
    @current_code = Code.parse(gets.chomp)
  end

  def display_matches(code)
    puts "near matches: #{@secret_code.near_matches(code)}"
    puts "exact matches: #{@secret_code.exact_matches(code)}"
  end


end
